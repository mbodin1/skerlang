(** This file was automatically generated using necroml
    See https://gitlab.inria.fr/skeletons/necro-ml/ for more informations *)

(** The unspecified types *)
module type TYPES = sig
  type int
end

(** The interpretation monad *)
module type MONAD = sig
  type 'a t
  val ret: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val branch: (unit -> 'a t) list -> 'a t
  val fail: string -> 'a t
  type kind = | Anon | Spec of string | Unspec of string
  val apply: kind -> ('a -> 'b t) -> 'a -> 'b t
  val extract: 'a t -> 'a
end

(** All types, and the unspecified values *)
module type UNSPEC = sig
  module M: MONAD
  include TYPES

  type 'a list =
  | Nil
  | Cons of ('a * 'a list)
  and expr =
  | Start
  | Spawn of (fidt * expr list)
  | SendMessage of (expr * expr)
  | Receive
  | Nodes
  and fidt = int
  and pidt = int
  and value = int

  val getMessage: (pidt -> value M.t)
  val parallel: ('a -> (('a -> 'b M.t) -> 'b M.t) M.t)
  val ret: ('a -> 'a M.t)
  val sendMessage: (pidt -> (pidt -> (value -> unit M.t) M.t) M.t)
end

(** A default instantiation *)
module Unspec (M:MONAD) (T:TYPES) = struct
  exception NotImplemented of string
  include T
  module M = M

  type 'a list =
  | Nil
  | Cons of ('a * 'a list)
  and expr =
  | Start
  | Spawn of (fidt * expr list)
  | SendMessage of (expr * expr)
  | Receive
  | Nodes
  and fidt = int
  and pidt = int
  and value = int

  let getMessage _ = raise (NotImplemented "getMessage")
  let parallel _ = raise (NotImplemented "parallel")
  let ret _ = raise (NotImplemented "ret")
  let sendMessage _ = raise (NotImplemented "sendMessage")
end


(** The module type for interpreters *)
module type INTERPRETER = sig
  module M:MONAD
  type int

  type 'a list =
  | Nil
  | Cons of ('a * 'a list)
  and expr =
  | Start
  | Spawn of (fidt * expr list)
  | SendMessage of (expr * expr)
  | Receive
  | Nodes
  and fidt = int
  and pidt = int
  and value = int

  val execute: (pidt -> (expr -> value M.t) M.t)
  val getMessage: (pidt -> value M.t)
  val parallel: ('a -> (('a -> 'b M.t) -> 'b M.t) M.t)
  val ret: ('a -> 'a M.t)
  val sendMessage: (pidt -> (pidt -> (value -> unit M.t) M.t) M.t)
end

(** Module defining the specified values *)
module MakeInterpreter (F : UNSPEC) = struct
  include F
  let ( let* ) = M.bind

  let apply1 k f x = M.apply k f x
  let apply2 k f x0 x1 =
    let* _tmp = apply1 k f x0 in
    apply1 M.Anon _tmp x1
  let apply3 k f x0 x1 x2 =
    let* _tmp = apply1 k f x0 in
    apply2 M.Anon _tmp x1 x2

  let rec execute =
     begin function pid0 ->
      M.ret (begin function e ->
        begin match  e  with
        | SendMessage (e1, e2) ->
            let* _tmp = 
              apply2 (M.Spec "execute") execute pid0 e1
            in
            apply2 (Unspec "parallel") parallel _tmp (function pid ->
            let* _tmp = 
              apply2 (M.Spec "execute") execute pid0 e2
            in
            apply2 (Unspec "parallel") parallel _tmp (function m ->
            let* _ =
              apply3 (M.Unspec "sendMessage") sendMessage pid0 pid m
            in
            M.ret m))
        | Receive -> apply1 (M.Unspec "getMessage") getMessage pid0
        | _ -> M.fail ""
        end
      end)
    end
end

